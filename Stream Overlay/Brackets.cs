﻿using System.Collections.Generic;

namespace Brackets
{
    public class Player
    {
        public int id { get; set; }
        public string name { get; set; }
        public string flag_name { get; set; }
        public string flag_image { get; set; }
        public string avatar_image { get; set; }
        public double power_rating { get; set; }
        public double elo { get; set; }
        public int seed { get; set; }
        public bool pre_seeded { get; set; }
    }

    public class MapPool
    {
        public int order { get; set; }
        public int map_id { get; set; }
    }

    public class Player2
    {
        public int? group_position { get; set; }
        public int player_id { get; set; }
        public int? score { get; set; }
        public bool? won { get; set; }
    }

    public class Match
    {
        public int id { get; set; }
        public int? group_id { get; set; }
        public bool is_bye { get; set; }
        public List<Player2> players { get; set; }
        public List<Schedule> schedule { get; set; }
    }

    public class Schedule
    {
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string channel_name { get; set; }
        public List<string> casters { get; set; }
        public string type { get; set; }
        public int? player_id { get; set; }
    }

    public class Round
    {
        public int id { get; set; }
        public string name { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public int best_of { get; set; }
        public List<MapPool> map_pool { get; set; }
        public List<Match> matches { get; set; }
    }

    public class Map
    {
        public string name { get; set; }
        public string minimap_image { get; set; }
    }

    public class CurrentBrackets
    {
        public string id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public List<Player> players { get; set; }
        public List<Round> rounds { get; set; }
        public List<Map> maps { get; set; }
    }
}
