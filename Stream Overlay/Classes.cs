﻿using System.Collections.Generic;

namespace Stream_Overlay
{
    public class Donation
    {
        public int donation_id { get; set; }
        public int created_at { get; set; }
        public string currency { get; set; }
        public string amount { get; set; }
        public string name { get; set; }
        public string message { get; set; }
        public string email { get; set; }
    }

    public class Donations
    {
        public List<Donation> data { get; set; }
    }

    public class DInfo
    {
        public string name { get; set; }
        public double amount { get; set; }
    }


    public class Tournament
    {
        public string id { get; set; }
        public string name { get; set; }
        public string forum_id { get; set; }
        public string prize_pool { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string game_type { get; set; }
        public string game_name { get; set; }
        public string patch_type { get; set; }
        public string patch_name { get; set; }
        public string patch_version { get; set; }
        public string status { get; set; }
        public List<Stage> stages { get; set; }
    }

    public class Stage
    {
        public string id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string is_active { get; set; }
        public string external_url { get; set; }
    }

    public class DisplayingMatch
    {
        public bool isInterview { get; set; }
        public string InterviewTitle { get; set; }
        public string p1 { get; set; }
        public string p2 { get; set; }
        public string f1 { get; set; }
        public string f2 { get; set; }
        public string round { get; set; }
        public string bracketTitle { get; set; }
        public string time { get; set; }
        public string casters { get; set; }
        public string a1 { get; set; }
        public string a2 { get; set; }
        public string BO { get; set; }
        public Dictionary<string, string> maps { get; set; }
        public double rank1 { get; set; }
        public double rank2 { get; set; }
    }

    public class BettingMatch
    {
        public string p1 { get; set; }
        public string p2 { get; set; }
        public double? odds1 { get; set; }
        public double? odds2 { get; set; }
    }
    }
