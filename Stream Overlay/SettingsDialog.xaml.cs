﻿using Brackets;
using Groups;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Stream_Overlay
{
    /// <summary>
    /// Логика взаимодействия для SettingsDialog.xaml
    /// </summary>
    public partial class SettingsDialog : Window, INotifyPropertyChanged
    {

        private Cursor FAoE { get; set; }
        public Cursor AoE
        {
            get { return FAoE; }
            set
            {
                FAoE = value;
                NotifyPropertyChanged("AoE");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));

        }

        public Maps MapPool = new Maps();
        public Maps SelectedMaps = new Maps();

        public Maps ManualMapPool = new Maps();
        public Maps ManualSelectedMaps = new Maps();


        public DispatcherTimer AnimateTips = new DispatcherTimer();
        SoundPlayer Click = new SoundPlayer(Application.GetResourceStream(new Uri("Elements/Click.wav", UriKind.Relative)).Stream);
        public SettingsDialog()
        {
            Timeline.DesiredFrameRateProperty.OverrideMetadata(
    typeof(Timeline),
    new FrameworkPropertyMetadata { DefaultValue = 30 });
            InitializeComponent();
            DataContext = this;

            AoE = new Cursor(Application.GetResourceStream(new Uri("pack://application:,,,/Elements/Cursor.cur")).Stream);



            if (File.Exists("OverlayList.json"))
            {
                string json = File.ReadAllText("OverlayList.json");
                Overlays overlays = JsonConvert.DeserializeObject<Overlays>(json);
                if (overlays.overlays.Any())
                {
                    foreach (Overlay o in overlays.overlays)
                        OverlayList.Items.Add(new ComboBoxItem() { Content = o.title, Tag = o });
                    OverlayList.SelectedIndex = 0;
                }
                else
                {
                    bLaunch.IsEnabled = false;
                }
            }
            else
            {
                bLaunch.IsEnabled = false;
            }


            if (File.Exists("MapPool.json"))
            {
                string json = File.ReadAllText("MapPool.json");
                MapPool = JsonConvert.DeserializeObject<Maps>(json);               
                lvMapPool.ItemsSource = MapPool.maps;

                ManualMapPool = JsonConvert.DeserializeObject<Maps>(json);
                lvManualMapPool.ItemsSource = ManualMapPool.maps;
            }
            lvSelectedMaps.ItemsSource = SelectedMaps.maps;
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(lvMapPool.ItemsSource);
            collectionView.SortDescriptions.Add(new SortDescription("title", ListSortDirection.Ascending));
            var view = (ICollectionViewLiveShaping)CollectionViewSource.GetDefaultView(lvMapPool.ItemsSource);
            view.IsLiveSorting = true;

            ICollectionView collectionView2 = CollectionViewSource.GetDefaultView(lvSelectedMaps.ItemsSource);
            collectionView2.SortDescriptions.Add(new SortDescription("title", ListSortDirection.Ascending));
            var view2 = (ICollectionViewLiveShaping)CollectionViewSource.GetDefaultView(lvSelectedMaps.ItemsSource);
            view2.IsLiveSorting = true;



            lvManualSelectedMaps.ItemsSource = ManualSelectedMaps.maps;
            ICollectionView collectionView3 = CollectionViewSource.GetDefaultView(lvManualMapPool.ItemsSource);
            collectionView3.SortDescriptions.Add(new SortDescription("title", ListSortDirection.Ascending));
            var view3 = (ICollectionViewLiveShaping)CollectionViewSource.GetDefaultView(lvManualMapPool.ItemsSource);
            view3.IsLiveSorting = true;

            ICollectionView collectionView4 = CollectionViewSource.GetDefaultView(lvManualSelectedMaps.ItemsSource);
            collectionView4.SortDescriptions.Add(new SortDescription("title", ListSortDirection.Ascending));
            var view4 = (ICollectionViewLiveShaping)CollectionViewSource.GetDefaultView(lvManualSelectedMaps.ItemsSource);
            view4.IsLiveSorting = true;



            AnimateTips.Interval = new TimeSpan(0, 0, 0);
            AnimateTips.Tick += async (object s, EventArgs eventArgs) =>
            {
                AnimateTips.Stop();
                DoubleAnimation center = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(2));
                CircleEase ease1 = new CircleEase();
                ease1.EasingMode = EasingMode.EaseInOut;
                center.EasingFunction = ease1;

                DoubleAnimation center2 = new DoubleAnimation(0, 1, TimeSpan.FromSeconds(2.2));
                center2.EasingFunction = ease1;
                DoubleAnimation center3 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(2.2));
                center3.EasingFunction = ease1;
                center3.BeginTime = TimeSpan.FromSeconds(10);
                DoubleAnimation center4 = new DoubleAnimation(1, 0, TimeSpan.FromSeconds(2));
                center4.EasingFunction = ease1;
                center4.BeginTime = TimeSpan.FromSeconds(10);


                ObjectAnimationUsingKeyFrames center5 = new ObjectAnimationUsingKeyFrames();
                center5.BeginTime = TimeSpan.FromSeconds(12.2);
                DiscreteObjectKeyFrame keyFrame = new DiscreteObjectKeyFrame(Visibility.Hidden, TimeSpan.FromSeconds(0));
                center5.KeyFrames.Add(keyFrame);

                ObjectAnimationUsingKeyFrames center6 = new ObjectAnimationUsingKeyFrames();
                center6.BeginTime = TimeSpan.FromSeconds(0);
                DiscreteObjectKeyFrame keyFrame1 = new DiscreteObjectKeyFrame(Visibility.Visible, TimeSpan.FromSeconds(0));
                center6.KeyFrames.Add(keyFrame1);

                for (int i = 1; i < 10; i++)
                {
                    switch (i)
                    {
                        case 1:
                            {
                                Tip9.Visibility = Visibility.Hidden;
                                Tip1.Visibility = Visibility.Visible;
                                break;
                            }
                        case 2:
                            {
                                Tip1.Visibility = Visibility.Hidden;
                                Tip2.Visibility = Visibility.Visible;
                                break;
                            }
                        case 3:
                            {
                                Tip2.Visibility = Visibility.Hidden;
                                Tip3.Visibility = Visibility.Visible;
                                break;
                            }
                        case 4:
                            {
                                Tip3.Visibility = Visibility.Hidden;
                                Tip4.Visibility = Visibility.Visible;
                                break;
                            }
                        case 5:
                            {
                                Tip4.Visibility = Visibility.Hidden;
                                Tip5.Visibility = Visibility.Visible;
                                break;
                            }
                        case 6:
                            {
                                Tip5.Visibility = Visibility.Hidden;
                                Tip6.Visibility = Visibility.Visible;
                                break;
                            }
                        case 7:
                            {
                                Tip6.Visibility = Visibility.Hidden;
                                Tip7.Visibility = Visibility.Visible;
                                break;
                            }
                        case 8:
                            {
                                Tip7.Visibility = Visibility.Hidden;
                                Tip8.Visibility = Visibility.Visible;
                                break;
                            }
                        case 9:
                            {
                                Tip8.Visibility = Visibility.Hidden;
                                Tip9.Visibility = Visibility.Visible;
                                break;
                            }
                    }
                    gTips.BeginAnimation(Grid.VisibilityProperty, center6);
                    TransparentStop.BeginAnimation(GradientStop.OffsetProperty, center, HandoffBehavior.Compose);
                    BlackStop.BeginAnimation(GradientStop.OffsetProperty, center2, HandoffBehavior.Compose);
                    TransparentStop.BeginAnimation(GradientStop.OffsetProperty, center3, HandoffBehavior.Compose);
                    BlackStop.BeginAnimation(GradientStop.OffsetProperty, center4, HandoffBehavior.Compose);
                    gTips.BeginAnimation(Grid.VisibilityProperty, center5, HandoffBehavior.Compose);
                    await Task.Delay(12200);
                }
                AnimateTips.Start();
            };
            AnimateTips.Start();

            if (File.Exists("UpdateCounter.txt"))
            {
                int counter = Convert.ToInt32(File.ReadAllText("UpdateCounter.txt"));
                if (counter < Counter)
                {
                    showGuide();
                }
            }
            else
            {
                showGuide();
            }

        }

        private void showGuide()
        {
            Dispatcher.Invoke(async () =>
            {
                tbHello.Opacity = 0;
                tbReadGuide.Opacity = 0;
                tbEnjoy.Opacity = 0;
                gHelp.Opacity = 0;
                bHelp.IsChecked = true;
                bHelp.IsEnabled = false;
                bClose.IsEnabled = false;
                await Task.Delay(3000);
                File.WriteAllText("UpdateCounter.txt", Counter.ToString());
                CircleEase ease = new CircleEase();
                ease.EasingMode = EasingMode.EaseInOut;
                DoubleAnimation a = new DoubleAnimation(0, 1, TimeSpan.FromMilliseconds(1000));
                a.EasingFunction = ease;
                DoubleAnimation b = new DoubleAnimation(1, 0, TimeSpan.FromMilliseconds(1000));
                b.EasingFunction = ease;
                tbHello.BeginAnimation(OpacityProperty, a);
                await Task.Delay(3000);
                tbHello.BeginAnimation(OpacityProperty, b);
                await Task.Delay(3000);
                tbReadGuide.BeginAnimation(OpacityProperty, a);
                await Task.Delay(3000);
                tbReadGuide.BeginAnimation(OpacityProperty, b);
                await Task.Delay(3000);
                gHelp.BeginAnimation(OpacityProperty, a);
                await Task.Delay(15000);
                gHelp.BeginAnimation(OpacityProperty, b);
                await Task.Delay(3000);
                gHelp.GoToPage(3);
                gHelp.BeginAnimation(OpacityProperty, a);
                while (gHelp.CanGoToNextPage)
                {
                    await Task.Delay(20000);
                    gHelp.BeginAnimation(OpacityProperty, b);
                    await Task.Delay(3000);
                    gHelp.NextPage();
                    gHelp.BeginAnimation(OpacityProperty, a);
                }
                await Task.Delay(20000);
                gHelp.BeginAnimation(OpacityProperty, b);
                await Task.Delay(2000);
                tbEnjoy.BeginAnimation(OpacityProperty, a);
                await Task.Delay(3000);
                tbEnjoy.BeginAnimation(OpacityProperty, b);
                await Task.Delay(2000);
                bHelp.IsChecked = false;
                bHelp.IsEnabled = true;
                bClose.IsEnabled = true;
                gHelp.FirstPage();
                gHelp.BeginAnimation(OpacityProperty, a);
            }
);
        }

        private static int Counter = 2;

        public async Task<string> getESOC(string url)
        {
            try
            {
                Uri uri = new Uri(url);
                HttpClientHandler handler = new HttpClientHandler();
                handler.CookieContainer = new CookieContainer();
                handler.CookieContainer.Add(uri, new Cookie("esocookie1_staff", ""));
                HttpClient client = new HttpClient(handler);
                client.DefaultRequestHeaders.Add("user-agent", "Stream-Overlay");
                HttpResponseMessage response = await client.GetAsync(uri);
                File.WriteAllText("last_request.txt", await response.Content.ReadAsStringAsync());
                response.EnsureSuccessStatusCode();
                return await response.Content.ReadAsStringAsync();
            }
            catch
            {
                return "error";
            }
        }

        List<Tournament> tournaments = new List<Tournament>();

        public CurrentGroups currentGroups1 = new CurrentGroups();
        public CurrentBrackets currentBrackets1 = new CurrentBrackets();

        public CurrentGroups currentGroups2 = new CurrentGroups();
        public CurrentBrackets currentBrackets2 = new CurrentBrackets();

        public CurrentGroups currentGroups3 = new CurrentGroups();
        public CurrentBrackets currentBrackets3 = new CurrentBrackets();

        public CurrentGroups currentGroups4 = new CurrentGroups();
        public CurrentBrackets currentBrackets4 = new CurrentBrackets();

        public CurrentBrackets WTcurrentBrackets = new CurrentBrackets();



        private void StackPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Button_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Click.Play();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        async Task<string> HttpGetAsync(string URI)
        {
            try
            {
                HttpClient hc = new HttpClient();
                Task<System.IO.Stream> result = hc.GetStreamAsync(URI);

                System.IO.Stream vs = await result;
                using (StreamReader am = new StreamReader(vs, Encoding.UTF8))
                {
                    return await am.ReadToEndAsync();
                }
            }
            catch
            {
                return "error";
            }
        }

        private async void Button_Click_1(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsEnabled = false;
            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "wt")
            {
                if (string.IsNullOrEmpty(WTRoundList.Text))
                {
                    MessageBox.Show("Please, select round.");
                    (sender as Button).IsEnabled = true;
                    return;
                }

                if (string.IsNullOrEmpty(WTPlayer1.Text) || string.IsNullOrEmpty(WTPlayer1.Text))
                {
                    MessageBox.Show("Please, select player names.");
                    (sender as Button).IsEnabled = true;
                    return;
                }


            }
            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "mu")
            {
                if (string.IsNullOrEmpty(tbPlayer1.Text) || string.IsNullOrEmpty(tbPlayer1.Text))
                {
                    MessageBox.Show("Please, enter player names.");
                    (sender as Button).IsEnabled = true;
                    return;
                }
                List<Task<string>> tasks = new List<Task<string>>();
                tasks.Add(HttpGetAsync("http://www.agecommunity.com/query/query.aspx?md=user&name=" + WebUtility.UrlEncode(tbPlayer1.Text)));
                tasks.Add(HttpGetAsync("http://www.agecommunity.com/query/query.aspx?md=user&name=" + WebUtility.UrlEncode(tbPlayer2.Text)));
                await Task.WhenAll(tasks);
                UserStatus US = new UserStatus(tbPlayer1.Text);
                US.Get(tasks[0].Result);
                if (US.ERROR)
                {
                    MessageBox.Show("You entered the wrong Player 1 name.");
                    (sender as Button).IsEnabled = true;
                    return;
                }
                tbPlayer1.Text = US.Nick;
                tbPlayer1.Tag = new KeyValuePair<string, string>(US.PRYS, US.Avatar);

                US = new UserStatus(tbPlayer2.Text);
                US.Get(tasks[1].Result);
                if (US.ERROR)
                {
                    MessageBox.Show("You entered the wrong Player 2 name.");
                    (sender as Button).IsEnabled = true;
                    return;
                }
                tbPlayer2.Text = US.Nick;
                tbPlayer2.Tag = new KeyValuePair<string, string>(US.PRYS, US.Avatar);

            }
            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "manual")
            {
                if (string.IsNullOrEmpty(ManualRoundList.Text))
                {
                    MessageBox.Show("Please, select round.");
                    (sender as Button).IsEnabled = true;
                    return;
                }

                if (string.IsNullOrEmpty(tbManualPlayer1.Text) || string.IsNullOrEmpty(tbManualPlayer1.Text))
                {
                    MessageBox.Show("Please, enter player names.");
                    (sender as Button).IsEnabled = true;
                    return;
                }
                List<Task<string>> tasks = new List<Task<string>>();
                tasks.Add(HttpGetAsync("http://www.agecommunity.com/query/query.aspx?md=user&name=" + WebUtility.UrlEncode(tbManualPlayer1.Text)));
                tasks.Add(HttpGetAsync("http://www.agecommunity.com/query/query.aspx?md=user&name=" + WebUtility.UrlEncode(tbManualPlayer2.Text)));
                await Task.WhenAll(tasks);
                UserStatus US = new UserStatus(tbManualPlayer1.Text);
                US.Get(tasks[0].Result);
                if (US.ERROR)
                {
                    MessageBox.Show("You entered the wrong Player 1 name.");
                    (sender as Button).IsEnabled = true;
                    return;
                }
                tbManualPlayer1.Text = US.Nick;
                tbManualPlayer1.Tag = new KeyValuePair<string, string>(US.PRYS, US.Avatar);

                US = new UserStatus(tbManualPlayer2.Text);
                US.Get(tasks[1].Result);
                if (US.ERROR)
                {
                    MessageBox.Show("You entered the wrong Player 2 name.");
                    (sender as Button).IsEnabled = true;
                    return;
                }
                tbManualPlayer2.Text = US.Nick;
                tbManualPlayer2.Tag = new KeyValuePair<string, string>(US.PRYS, US.Avatar);

            }
            this.Hide();
            Checklist checklist = new Checklist();
            checklist.ShowDialog();
            (sender as Button).IsEnabled = true;
        }

        private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
            Process.Start(e.Uri.ToString());
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            gMatch1.Visibility = Visibility.Visible;
        }

        private void ToggleButton_Checked_1(object sender, RoutedEventArgs e)
        {
            gMatch2.Visibility = Visibility.Visible;
        }

        private void ToggleButton_Checked_2(object sender, RoutedEventArgs e)
        {
            gMatch3.Visibility = Visibility.Visible;
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            gMatch3.Visibility = Visibility.Collapsed;
            BracketGroupList3.SelectedIndex = -1;
            RoundList3.Items.Clear();
            MatchList3.Items.Clear();
        }

        private void ToggleButton_Unchecked_1(object sender, RoutedEventArgs e)
        {
            gMatch2.Visibility = Visibility.Collapsed;
            BracketGroupList2.SelectedIndex = -1;
            RoundList2.Items.Clear();
            MatchList2.Items.Clear();
        }

        private void ToggleButton_Unchecked_2(object sender, RoutedEventArgs e)
        {
            gMatch1.Visibility = Visibility.Collapsed;
            BracketGroupList1.SelectedIndex = -1;
            RoundList1.Items.Clear();
            MatchList1.Items.Clear();
        }

        private void eBrackets_Unchecked(object sender, RoutedEventArgs e)
        {
            BracketGroupList1.Items.Clear();
            BracketGroupList2.Items.Clear();
            BracketGroupList3.Items.Clear();
            BracketGroupList4.Items.Clear();
            bAdd1.IsChecked = false;
            bAdd2.IsChecked = false;
            bAdd3.IsChecked = false;
            bAdd4.IsChecked = false;

            bAdd1.IsEnabled = false;
            bAdd2.IsEnabled = false;
            bAdd3.IsEnabled = false;
            bAdd4.IsEnabled = false;
        }

        private async void eBrackets_Checked(object sender, RoutedEventArgs e)
        {
            eBrackets.IsEnabled = false;
            bAdd1.IsChecked = false;
            bAdd2.IsChecked = false;
            bAdd3.IsChecked = false;
            bAdd4.IsChecked = false;

            bAdd1.IsEnabled = false;
            bAdd2.IsEnabled = false;
            bAdd3.IsEnabled = false;
            bAdd4.IsEnabled = false;
            string json = await getESOC("https://dev.eso-community.net/events.json?assoc=true");
            try
            {
                tournaments = JsonConvert.DeserializeObject<List<Tournament>>(json);
                foreach (Tournament t in tournaments)
                {
                    //if (t.status == "In-Progress")
                    {
                        foreach (Stage s in t.stages)
                        {
                            BracketGroupList1.Items.Add(new ComboBoxItem() { Content = t.name + " " + s.name, Tag = "https://dev.eso-community.net/tournament/" + t.id.ToString() + "/" + s.id.ToString() + ".json?assoc=true" });
                            BracketGroupList2.Items.Add(new ComboBoxItem() { Content = t.name + " " + s.name, Tag = "https://dev.eso-community.net/tournament/" + t.id.ToString() + "/" + s.id.ToString() + ".json?assoc=true" });
                            BracketGroupList3.Items.Add(new ComboBoxItem() { Content = t.name + " " + s.name, Tag = "https://dev.eso-community.net/tournament/" + t.id.ToString() + "/" + s.id.ToString() + ".json?assoc=true" });
                            BracketGroupList4.Items.Add(new ComboBoxItem() { Content = t.name + " " + s.name, Tag = "https://dev.eso-community.net/tournament/" + t.id.ToString() + "/" + s.id.ToString() + ".json?assoc=true" });

                        }
                    }
                }
                if (tournaments.Any())
                {
                    bAdd1.IsEnabled = true;
                    bAdd2.IsEnabled = true;
                    bAdd3.IsEnabled = true;
                    bAdd4.IsEnabled = true;
                }
            }
            catch
            {
                bAdd1.IsChecked = false;
                bAdd2.IsChecked = false;
                bAdd3.IsChecked = false;
                bAdd4.IsChecked = false;

                bAdd1.IsEnabled = false;
                bAdd2.IsEnabled = false;
                bAdd3.IsEnabled = false;
                bAdd4.IsEnabled = false;
                eBrackets.IsChecked = false;
                MessageBox.Show("ESOC API is not working now. Please contact with ESOC devs.");
            }
            eBrackets.IsEnabled = true;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.System)
            {
                e.Handled = true;
            }
        }


        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://eso-community.net/");
        }



        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Directory.CreateDirectory("Thumbnails");
            string p1 = "";
            string p2 = "";
            bool secondChance = false;
            string Name = ((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).thumbnail;
            if (string.IsNullOrEmpty(Name))
            {
                MessageBox.Show("You can not create thumbnail for this overlay.");
                return;
            }

            string event_type = ((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type;


            if (event_type == "tournament")
            {
                if (MatchList1.SelectedItem == null)
                {
                    MessageBox.Show("Select match firstly !");
                    return;
                }

                if (BracketGroupList1.Text.Contains("Groups Qualifiers") || BracketGroupList1.Text.Contains("Groups Placements"))
                {
                    p1 = currentGroups1.players.Single(s => s.id == ((RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)(MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name;
                    p2 = currentGroups1.players.Single(s => s.id == ((RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>).Single(m => m.id == (int)(MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name;

                }
                else
                if (BracketGroupList1.Text.Contains("Elimination Brackets"))
                {
                    p1 = currentBrackets1.players.Single(s => s.id == ((RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)(MatchList1.SelectedItem as ComboBoxItem).Tag).players[0].player_id).name;
                    p2 = currentBrackets1.players.Single(s => s.id == ((RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches.Single(m => m.id == (int)(MatchList1.SelectedItem as ComboBoxItem).Tag).players[1].player_id).name;

                }
                secondChance = BracketGroupList1.Text.Contains("Second Chance");
            }
            else
            if (event_type == "mu")
            {
                p1 = tbPlayer1.Text;
                p2 = tbPlayer2.Text;
                if (string.IsNullOrEmpty(p1) || string.IsNullOrEmpty(p2))
                {
                    MessageBox.Show("Please, enter player names.");
                    return;
                }
            }
            if (event_type == "manual")
            {
                p1 = tbManualPlayer1.Text;
                p2 = tbManualPlayer2.Text;
                if (string.IsNullOrEmpty(p1) || string.IsNullOrEmpty(p2))
                {
                    MessageBox.Show("Please, enter player names.");
                    return;
                }
            }
            else
            if (event_type == "wt")
            {
                p1 = WTPlayer1.Text;
                p2 = WTPlayer2.Text;
                if (string.IsNullOrEmpty(p1) || string.IsNullOrEmpty(p2))
                {
                    MessageBox.Show("Please, select player names.");
                    return;
                }
            }
            else
                return;
            BitmapImage bi = new BitmapImage(new Uri(Path.Combine(Environment.CurrentDirectory, Name)));
            FormattedText player1 = new FormattedText(p1,
                new CultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Formal436 BT"), FontStyles.Normal, FontWeights.UltraBlack, new FontStretch()),
                96,
                Brushes.White);
            FormattedText player2 = new FormattedText(p2,
                new CultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Formal436 BT"), FontStyles.Normal, FontWeights.UltraBlack, new FontStretch()),
                96,
                Brushes.White);
            FormattedText vs = new FormattedText("vs",
                new CultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Formal436 BT"), FontStyles.Normal, FontWeights.UltraBlack, new FontStretch()),
                82,
                Brushes.White);
            FormattedText title = new FormattedText(RoundList1.Text,
                new CultureInfo("en-us"),
                FlowDirection.LeftToRight,
                new Typeface(new FontFamily(new Uri("pack://application:,,,/"), "./Fonts/#Formal436 BT"), FontStyles.Normal, FontWeights.UltraBlack, new FontStretch()),
                72,
                Brushes.White);


            PathGeometry n = new PathGeometry();
            Geometry player1Geometry = player1.BuildGeometry(new Point((bi.PixelWidth - player1.Width) / 2, 360));
            Geometry player2Geometry = player2.BuildGeometry(new Point((bi.PixelWidth - player2.Width) / 2, 530));

            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();
            drawingContext.DrawImage(bi, new Rect(0, 0, bi.PixelWidth, bi.PixelHeight));
            drawingContext.DrawText(vs, new Point((bi.PixelWidth - vs.Width) / 2, 455));
            drawingContext.DrawGeometry(Brushes.White, new Pen(Brushes.Black, 2), player1Geometry);
            drawingContext.DrawGeometry(Brushes.White, new Pen(Brushes.Black, 2), player2Geometry);
            drawingContext.DrawText(title, new Point((bi.PixelWidth - title.Width) / 2, 353 - title.Height - (90 - title.Height) / 2));
            /* if (secondChance)
             {
                 var overlayImage = new BitmapImage(new Uri("pack://application:,,,/Elements/SecondChance.png"));
                 drawingContext.DrawImage(overlayImage, new Rect(bi.Width - overlayImage.Width, 0, overlayImage.Width, overlayImage.Height));

             }*/
            drawingContext.Close();

            RenderTargetBitmap rtb = new RenderTargetBitmap(bi.PixelWidth, bi.PixelHeight, 96, 96, PixelFormats.Pbgra32);
            rtb.Render(drawingVisual);

            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(rtb));
            using (Stream stm = File.Create(Path.Combine(Environment.CurrentDirectory, "Thumbnails", "[" + OverlayList.Text + " - " + RoundList1.Text + "] " + p1 + " vs. " + p2 + ".png")))
            {
                png.Save(stm);
            }
            Process.Start(new ProcessStartInfo()
            {
                FileName = "explorer",
                Arguments = "/e, /select, \"" + Path.Combine(Environment.CurrentDirectory, "Thumbnails", "[" + OverlayList.Text + " - " + RoundList1.Text + "] " + MatchList1.Text + ".png") + "\""
            });
            Process.Start(new ProcessStartInfo()
            {
                FileName = Path.Combine(Environment.CurrentDirectory, "Thumbnails", "[" + OverlayList.Text + " - " + RoundList1.Text + "] " + MatchList1.Text + ".png"),
                UseShellExecute = true,
                Verb = "open"
            });
        }

        private async void BracketGroupList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (BracketGroupList1.SelectedItem != null)
            {

                RoundList1.Items.Clear();
                MatchList1.Items.Clear();
                string json = await getESOC((BracketGroupList1.SelectedItem as ComboBoxItem).Tag.ToString());
                try
                {
                    if (BracketGroupList1.Text.Contains("Groups Qualifiers") || BracketGroupList1.Text.Contains("Groups Placements"))
                    {
                        currentGroups1 = new CurrentGroups();
                        currentGroups1 = JsonConvert.DeserializeObject<CurrentGroups>(json);
                        foreach (Group g in currentGroups1.groups)
                        {
                            string GroupIndex;
                            if (BracketGroupList1.Text.Contains("Groups Placements"))
                                GroupIndex = "Group " + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[g.id - 1];
                            else
                                GroupIndex = "Group " + g.id.ToString();
                            RoundList1.Items.Add(new ComboBoxItem() { Content = GroupIndex, Tag = g.matches });
                        }
                    }
                    else
                    if (BracketGroupList1.Text.Contains("Elimination Brackets"))
                    {
                        currentBrackets1 = new CurrentBrackets();
                        currentBrackets1 = JsonConvert.DeserializeObject<CurrentBrackets>(json);
                        foreach (Brackets.Round r in currentBrackets1.rounds)
                        {
                            RoundList1.Items.Add(new ComboBoxItem() { Content = r.name, Tag = r });
                        }

                    }
                }
                catch
                {
                }
            }
        }

        private async void BracketGroupList2_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (BracketGroupList2.SelectedItem != null)
            {

                RoundList2.Items.Clear();
                MatchList2.Items.Clear();
                string json = await getESOC((BracketGroupList2.SelectedItem as ComboBoxItem).Tag.ToString());
                try
                {
                    if (BracketGroupList2.Text.Contains("Groups Qualifiers") || BracketGroupList2.Text.Contains("Groups Placements"))
                    {
                        currentGroups2 = new CurrentGroups();
                        currentGroups2 = JsonConvert.DeserializeObject<CurrentGroups>(json);
                        foreach (Group g in currentGroups2.groups)
                        {
                            string GroupIndex;
                            if (BracketGroupList2.Text.Contains("Groups Placements"))
                                GroupIndex = "Group " + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[g.id - 1];
                            else
                                GroupIndex = "Group " + g.id.ToString();
                            RoundList2.Items.Add(new ComboBoxItem() { Content = GroupIndex, Tag = g.matches });
                        }
                    }
                    else
                    if (BracketGroupList2.Text.Contains("Elimination Brackets"))
                    {
                        currentBrackets2 = new CurrentBrackets();
                        currentBrackets2 = JsonConvert.DeserializeObject<CurrentBrackets>(json);
                        foreach (Brackets.Round r in currentBrackets2.rounds)
                        {
                            RoundList2.Items.Add(new ComboBoxItem() { Content = r.name, Tag = r });
                        }

                    }
                }
                catch
                {
                }
            }
        }

        private async void BracketGroupList3_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (BracketGroupList3.SelectedItem != null)
            {

                RoundList3.Items.Clear();
                MatchList3.Items.Clear();
                string json = await getESOC((BracketGroupList3.SelectedItem as ComboBoxItem).Tag.ToString());
                try
                {
                    if (BracketGroupList3.Text.Contains("Groups Qualifiers") || BracketGroupList3.Text.Contains("Groups Placements"))
                    {
                        currentGroups3 = new CurrentGroups();
                        currentGroups3 = JsonConvert.DeserializeObject<CurrentGroups>(json);
                        foreach (Group g in currentGroups3.groups)
                        {
                            string GroupIndex;
                            if (BracketGroupList3.Text.Contains("Groups Placements"))
                                GroupIndex = "Group " + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[g.id - 1];
                            else
                                GroupIndex = "Group " + g.id.ToString();
                            RoundList3.Items.Add(new ComboBoxItem() { Content = GroupIndex, Tag = g.matches });
                        }
                    }
                    else
                    if (BracketGroupList3.Text.Contains("Elimination Brackets"))
                    {
                        currentBrackets3 = new CurrentBrackets();
                        currentBrackets3 = JsonConvert.DeserializeObject<CurrentBrackets>(json);
                        foreach (Brackets.Round r in currentBrackets3.rounds)
                        {
                            RoundList3.Items.Add(new ComboBoxItem() { Content = r.name, Tag = r });
                        }

                    }
                }
                catch
                {
                }
            }
        }

        private async void BracketGroupList4_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            if (BracketGroupList4.SelectedItem != null)
            {

                RoundList4.Items.Clear();
                MatchList4.Items.Clear();
                string json = await getESOC((BracketGroupList4.SelectedItem as ComboBoxItem).Tag.ToString());
                try
                {
                    if (BracketGroupList4.Text.Contains("Groups Qualifiers") || BracketGroupList4.Text.Contains("Groups Placements"))
                    {
                        currentGroups4 = new CurrentGroups();
                        currentGroups4 = JsonConvert.DeserializeObject<CurrentGroups>(json);
                        foreach (Group g in currentGroups4.groups)
                        {
                            string GroupIndex;
                            if (BracketGroupList4.Text.Contains("Groups Placements"))
                                GroupIndex = "Group " + "ABCDEFGHIJKLMNOPQRSTUVWXYZ"[g.id - 1];
                            else
                                GroupIndex = "Group " + g.id.ToString();
                            RoundList4.Items.Add(new ComboBoxItem() { Content = GroupIndex, Tag = g.matches });
                        }
                    }
                    else
                    if (BracketGroupList4.Text.Contains("Elimination Brackets"))
                    {
                        currentBrackets4 = new CurrentBrackets();
                        currentBrackets4 = JsonConvert.DeserializeObject<CurrentBrackets>(json);
                        foreach (Brackets.Round r in currentBrackets4.rounds)
                        {
                            RoundList4.Items.Add(new ComboBoxItem() { Content = r.name, Tag = r });
                        }

                    }
                }
                catch
                {
                }
            }
        }

        private void RoundList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (RoundList1.SelectedItem != null)
            {
                MatchList1.Items.Clear();
                try
                {
                    if (BracketGroupList1.Text.Contains("Groups Qualifiers") || BracketGroupList1.Text.Contains("Groups Placements"))
                    {

                        foreach (Groups.Match m in (RoundList1.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>)
                        {
                            MatchList1.Items.Add(new ComboBoxItem() { Content = currentGroups1.players.Single(s => s.id == m.players[0].player_id).name + " vs. " + currentGroups1.players.Single(s => s.id == m.players[1].player_id).name, Tag = m.id });
                        }
                    }
                    else
                    if (BracketGroupList1.Text.Contains("Elimination Brackets"))
                    {

                        foreach (Brackets.Match m in ((RoundList1.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches)
                        {
                            if (!string.IsNullOrEmpty(currentBrackets1.players.Single(s => s.id == m.players[0].player_id).name) && !string.IsNullOrEmpty(currentBrackets1.players.Single(s => s.id == m.players[1].player_id).name) && !m.is_bye)
                                MatchList1.Items.Add(new ComboBoxItem() { Content = currentBrackets1.players.Single(s => s.id == m.players[0].player_id).name + " vs. " + currentBrackets1.players.Single(s => s.id == m.players[1].player_id).name, Tag = m.id });

                        }
                    }
                }
                catch
                {
                    MatchList1.Items.Clear();
                }
            }
            else
            {
                MatchList1.Items.Clear();
            }
        }

        private void RoundList2_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (RoundList2.SelectedItem != null)
            {
                MatchList2.Items.Clear();
                try
                {
                    if (BracketGroupList2.Text.Contains("Groups Qualifiers") || BracketGroupList2.Text.Contains("Groups Placements"))
                    {

                        foreach (Groups.Match m in (RoundList2.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>)
                        {
                            MatchList2.Items.Add(new ComboBoxItem() { Content = currentGroups2.players.Single(s => s.id == m.players[0].player_id).name + " vs. " + currentGroups2.players.Single(s => s.id == m.players[1].player_id).name, Tag = m.id });
                        }
                    }
                    else
                    if (BracketGroupList2.Text.Contains("Elimination Brackets"))
                    {

                        foreach (Brackets.Match m in ((RoundList2.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches)
                        {
                            if (!string.IsNullOrEmpty(currentBrackets2.players.Single(s => s.id == m.players[0].player_id).name) && !string.IsNullOrEmpty(currentBrackets2.players.Single(s => s.id == m.players[1].player_id).name) && !m.is_bye)
                                MatchList2.Items.Add(new ComboBoxItem() { Content = currentBrackets2.players.Single(s => s.id == m.players[0].player_id).name + " vs. " + currentBrackets2.players.Single(s => s.id == m.players[1].player_id).name, Tag = m.id });

                        }
                    }
                }
                catch
                {
                    MatchList2.Items.Clear();
                }
            }
            else
            {
                MatchList2.Items.Clear();
            }
        }

        private void RoundList3_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (RoundList3.SelectedItem != null)
            {
                MatchList3.Items.Clear();
                try
                {
                    if (BracketGroupList3.Text.Contains("Groups Qualifiers") || BracketGroupList3.Text.Contains("Groups Placements"))
                    {

                        foreach (Groups.Match m in (RoundList3.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>)
                        {
                            MatchList3.Items.Add(new ComboBoxItem() { Content = currentGroups3.players.Single(s => s.id == m.players[0].player_id).name + " vs. " + currentGroups3.players.Single(s => s.id == m.players[1].player_id).name, Tag = m.id });
                        }
                    }
                    else
                    if (BracketGroupList3.Text.Contains("Elimination Brackets"))
                    {

                        foreach (Brackets.Match m in ((RoundList3.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches)
                        {
                            if (!string.IsNullOrEmpty(currentBrackets3.players.Single(s => s.id == m.players[0].player_id).name) && !string.IsNullOrEmpty(currentBrackets3.players.Single(s => s.id == m.players[1].player_id).name) && !m.is_bye)
                                MatchList3.Items.Add(new ComboBoxItem() { Content = currentBrackets3.players.Single(s => s.id == m.players[0].player_id).name + " vs. " + currentBrackets3.players.Single(s => s.id == m.players[1].player_id).name, Tag = m.id });

                        }
                    }
                }
                catch
                {
                    MatchList3.Items.Clear();
                }
            }
            else
            {
                MatchList3.Items.Clear();
            }
        }

        private void bAdd4_Checked(object sender, RoutedEventArgs e)
        {
            gMatch4.Visibility = Visibility.Visible;
        }

        private void bAdd4_Unchecked(object sender, RoutedEventArgs e)
        {
            gMatch4.Visibility = Visibility.Collapsed;
            BracketGroupList4.SelectedIndex = -1;
            RoundList4.Items.Clear();
            MatchList4.Items.Clear();
        }

        private void RoundList4_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (RoundList4.SelectedItem != null)
            {
                MatchList4.Items.Clear();
                try
                {
                    if (BracketGroupList4.Text.Contains("Groups Qualifiers") || BracketGroupList4.Text.Contains("Groups Placements"))
                    {

                        foreach (Groups.Match m in (RoundList4.SelectedItem as ComboBoxItem).Tag as List<Groups.Match>)
                        {
                            MatchList4.Items.Add(new ComboBoxItem() { Content = currentGroups4.players.Single(s => s.id == m.players[0].player_id).name + " vs. " + currentGroups4.players.Single(s => s.id == m.players[1].player_id).name, Tag = m.id });
                        }
                    }
                    else
                    if (BracketGroupList4.Text.Contains("Elimination Brackets"))
                    {

                        foreach (Brackets.Match m in ((RoundList4.SelectedItem as ComboBoxItem).Tag as Brackets.Round).matches)
                        {
                            if (!string.IsNullOrEmpty(currentBrackets4.players.Single(s => s.id == m.players[0].player_id).name) && !string.IsNullOrEmpty(currentBrackets4.players.Single(s => s.id == m.players[1].player_id).name) && !m.is_bye)
                                MatchList4.Items.Add(new ComboBoxItem() { Content = currentBrackets4.players.Single(s => s.id == m.players[0].player_id).name + " vs. " + currentBrackets4.players.Single(s => s.id == m.players[1].player_id).name, Tag = m.id });

                        }
                    }
                }
                catch
                {
                    MatchList4.Items.Clear();
                }
            }
            else
            {
                MatchList4.Items.Clear();
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            this.Topmost = false;
        }

        private void Window_Initialized(object sender, EventArgs e)
        {
            this.Topmost = true;
        }

        private void Window_Activated(object sender, EventArgs e)
        {
            foreach (Window window in App.Current.Windows)
            {
                if (window is Checklist || window is MainWindow)
                {
                    if (!window.IsVisible)
                    {
                        window.Close();
                    }
                }
            }
        }

        private void OverlayList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DoubleAnimation anim = new DoubleAnimation();
            anim.From = this.Height;
            anim.Duration = TimeSpan.FromMilliseconds(500);
            CircleEase ease = new CircleEase();
            ease.EasingMode = EasingMode.EaseInOut;
            anim.EasingFunction = ease;

            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "tournament")
            {
                gMU.Visibility = Visibility.Collapsed;
                gWeekendTours.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Visible;
                anim.To = 735;
            }
            else
            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "mu")
            {
                gWeekendTours.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Collapsed;
                gMU.Visibility = Visibility.Visible;
                anim.To = 735;
            }
            else
            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "manual")
            {
                gWeekendTours.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Collapsed;
                gMU.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Visible;
                anim.To = 735;
            }
            else
            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "wt")
            {
                gMU.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Collapsed;
                gWeekendTours.Visibility = Visibility.Visible;
                anim.To = 574;
            }
            else
            {
                gMU.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Collapsed;
                gWeekendTours.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Collapsed;
                anim.To = 362;
            }
            this.BeginAnimation(Window.HeightProperty, anim);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsEnabled = false;
            List<Map> selected = new List<Map>();
            foreach (var s in lvMapPool.SelectedItems)
            {
                selected.Add(s as Map);
            }
            foreach (var s in selected)
            {
                SelectedMaps.maps.Add(s);
                MapPool.maps.Remove(s);
            }

(sender as Button).IsEnabled = true;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsEnabled = false;
            List<Map> selected = new List<Map>();
            foreach (var s in lvSelectedMaps.SelectedItems)
            {
                selected.Add(s as Map);
            }
            foreach (var s in selected)
            {
                MapPool.maps.Add(s);
                SelectedMaps.maps.Remove(s);
            }


            (sender as Button).IsEnabled = true;
        }

        private async void WTBrackets_Checked(object sender, RoutedEventArgs e)
        {
            WTBrackets.IsEnabled = false;
            WTBracketGroupList.Items.Clear();
            WTRoundList.Items.Clear();
            WTPlayer1.Items.Clear();
            WTPlayer2.Items.Clear();

            gWT.IsEnabled = false;
            string json = await getESOC("https://dev.eso-community.net/events.json?assoc=true");
            try
            {
                tournaments = JsonConvert.DeserializeObject<List<Tournament>>(json);
                foreach (Tournament t in tournaments)
                {
                    //      if (t.status == "In-Progress")
                    {
                        foreach (Stage s in t.stages)
                        {
                            WTBracketGroupList.Items.Add(new ComboBoxItem() { Content = t.name + " " + s.name, Tag = "https://dev.eso-community.net/tournament/" + t.id.ToString() + "/" + s.id.ToString() + ".json?assoc=true" });
                        }
                    }
                }
                if (tournaments.Any())
                {
                    gWT.IsEnabled = true;
                }
            }
            catch
            {
                WTBracketGroupList.Items.Clear();
                WTRoundList.Items.Clear();
                WTPlayer1.Items.Clear();
                WTPlayer2.Items.Clear();

                gWT.IsEnabled = false;
                WTBrackets.IsChecked = false;
                MessageBox.Show("ESOC API is not working now. Please contact with ESOC devs.");
            }
            WTBrackets.IsEnabled = true;
        }

        private void WTBrackets_Unchecked(object sender, RoutedEventArgs e)
        {
            WTBracketGroupList.Items.Clear();
            WTRoundList.Items.Clear();
            WTPlayer1.Items.Clear();
            WTPlayer2.Items.Clear();

            gWT.IsEnabled = false;
        }

        private async void WTBracketGroupList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (WTBracketGroupList.SelectedItem != null)
            {

                WTRoundList.Items.Clear();
                WTPlayer1.Items.Clear();
                WTPlayer2.Items.Clear();
                string json = await getESOC((WTBracketGroupList.SelectedItem as ComboBoxItem).Tag.ToString());
                try
                {
                    WTcurrentBrackets = new CurrentBrackets();
                    WTcurrentBrackets = JsonConvert.DeserializeObject<CurrentBrackets>(json);
                    foreach (Brackets.Round r in WTcurrentBrackets.rounds)
                    {
                        WTRoundList.Items.Add(new ComboBoxItem() { Content = r.name, Tag = r });
                    }

                    foreach (Brackets.Player r in WTcurrentBrackets.players)
                    {
                        if (!r.name.Contains("Bye!"))
                        {
                            WTPlayer1.Items.Add(new ComboBoxItem() { Content = r.name, Tag = r });
                            WTPlayer2.Items.Add(new ComboBoxItem() { Content = r.name, Tag = r });
                        }
                    }

                }
                catch
                {
                }
            }
        }

        private void ToggleButton_Checked_3(object sender, RoutedEventArgs e)
        {

                DoubleAnimation anim = new DoubleAnimation();
                anim.From = this.Height;
                anim.Duration = TimeSpan.FromMilliseconds(500);
                CircleEase ease = new CircleEase();
                ease.EasingMode = EasingMode.EaseInOut;
                anim.EasingFunction = ease;
                gMain.Visibility = Visibility.Collapsed;
                gHelp.Visibility = Visibility.Visible;
                anim.To = 735;
                this.BeginAnimation(Window.HeightProperty, anim);


        }

        private void ToggleButton_Unchecked_3(object sender, RoutedEventArgs e)
        {

                gHelp.Visibility = Visibility.Collapsed;
            gMain.Visibility = Visibility.Visible;
                DoubleAnimation anim = new DoubleAnimation();
                anim.From = this.Height;
                anim.Duration = TimeSpan.FromMilliseconds(500);
                CircleEase ease = new CircleEase();
                ease.EasingMode = EasingMode.EaseInOut;
                anim.EasingFunction = ease;

            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "tournament")
            {
                gMU.Visibility = Visibility.Collapsed;
                gWeekendTours.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Visible;
                anim.To = 735;
            }
            else
             if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "mu")
            {
                gWeekendTours.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Collapsed;
                gMU.Visibility = Visibility.Visible;
                anim.To = 735;
            }
            else
            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "manual")
            {
                gWeekendTours.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Collapsed;
                gMU.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Visible;
                anim.To = 735;
            }
            else
            if (((OverlayList.SelectedItem as ComboBoxItem).Tag as Overlay).type == "wt")
            {
                gMU.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Collapsed;
                gWeekendTours.Visibility = Visibility.Visible;
                anim.To = 574;
            }
            else
            {
                gMU.Visibility = Visibility.Collapsed;
                gTournament.Visibility = Visibility.Collapsed;
                gWeekendTours.Visibility = Visibility.Collapsed;
                gManual.Visibility = Visibility.Collapsed;
                anim.To = 362;
            }
            this.BeginAnimation(Window.HeightProperty, anim);
            
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsEnabled = false;
            List<Map> selected = new List<Map>();
            foreach (var s in lvManualMapPool.SelectedItems)
            {
                selected.Add(s as Map);
            }
            foreach (var s in selected)
            {
                ManualSelectedMaps.maps.Add(s);
                ManualMapPool.maps.Remove(s);
            }

(sender as Button).IsEnabled = true;
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsEnabled = false;
            List<Map> selected = new List<Map>();
            foreach (var s in lvManualSelectedMaps.SelectedItems)
            {
                selected.Add(s as Map);
            }
            foreach (var s in selected)
            {
                ManualMapPool.maps.Add(s);
                ManualSelectedMaps.maps.Remove(s);
            }


            (sender as Button).IsEnabled = true;
        }
    }
}
