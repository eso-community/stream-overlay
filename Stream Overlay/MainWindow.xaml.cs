﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Threading;
using WPFMediaKit.DirectShow.Controls;

namespace Stream_Overlay
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private int score1 = 0;
        private int score2 = 0;

        private bool needClose = true;

        public string Score
        {
            get { return score1.ToString() + " - " + score2.ToString(); }
        }

        private string _wating = "waiting";
        public string Time
        {
            get
            {
                if (_time.TotalSeconds == 0)
                {
                    if (_wating == "waiting..")
                    {
                        _wating = "waiting...";
                        return "waiting...";
                    }
                    if (_wating == "waiting...")
                    {
                        _wating = "waiting";
                        return "waiting";
                    }
                    if (_wating == "waiting")
                    {
                        _wating = "waiting.";
                        return "waiting.";
                    }
                    else
                    {
                        _wating = "waiting..";
                        return "waiting..";
                    }
                }
                else
                {
                    _wating = "waiting";
                    return _time.ToString("c");
                }

            }
        }

        public class ActionCommand : ICommand
        {
            private readonly Action _action;

            public ActionCommand(Action action)
            {
                _action = action;
            }

            public void Execute(object parameter)
            {
                _action();
            }

            public bool CanExecute(object parameter)
            {
                return true;
            }

            public event EventHandler CanExecuteChanged;
        }

        DispatcherTimer _timer;
        TimeSpan _time;

        private ICommand timeUp;
        public ICommand TimeUp
        {
            get
            {
                return timeUp
                    ?? (timeUp = new ActionCommand(() =>
                    {
                        _time = _time.Add(TimeSpan.FromMinutes(1));
                        NotifyPropertyChanged("Time");
                    }));
            }
        }

        private ICommand timeDown;
        public ICommand TimeDown
        {
            get
            {
                return timeDown
                    ?? (timeDown = new ActionCommand(() =>
                    {
                        if (_time.TotalSeconds > 60)
                        {
                            _time = _time.Add(TimeSpan.FromMinutes(-1));
                            NotifyPropertyChanged("Time");
                        }
                    }));
            }
        }

        private ICommand score1Down;
        public ICommand Score1Down
        {
            get
            {
                return score1Down
                    ?? (score1Down = new ActionCommand(() =>
                    {
                        if (score1 > 0)
                        {
                            score1--;
                            NotifyPropertyChanged("Score");
                        }
                    }));
            }
        }

        private ICommand score2Down;
        public ICommand Score2Down
        {
            get
            {
                return score2Down
                    ?? (score2Down = new ActionCommand(() =>
                    {
                        if (score2 > 0)
                        {
                            score2--;
                            NotifyPropertyChanged("Score");
                        }
                    }));
            }
        }

        private ICommand score1Up;
        public ICommand Score1Up
        {
            get
            {
                return score1Up
                    ?? (score1Up = new ActionCommand(() =>
                    {

                        score1++;
                        NotifyPropertyChanged("Score");
                    }));
            }
        }

        private ICommand score2Up;
        public ICommand Score2Up
        {
            get
            {
                return score2Up
                    ?? (score2Up = new ActionCommand(() =>
                    {

                        score2++;
                        NotifyPropertyChanged("Score");
                    }));
            }
        }

        private ICommand closeOverlay;
        public ICommand CloseOverlay
        {
            get
            {
                return closeOverlay
                    ?? (closeOverlay = new ActionCommand(() =>
                    {
                    needClose = false;
                        ((Overlay.Content as FrameworkElement).FindName("Animation") as MediaUriElement).Stop();
                        ((Overlay.Content as FrameworkElement).FindName("Animation") as MediaUriElement).Close();
                        this.Hide();
                        Application.Current.MainWindow.Show();
                    }));
            }
        }

        private ICommand mininizeOverlay;
        public ICommand MinimizeOverlay
        {
            get
            {
                return mininizeOverlay
                    ?? (mininizeOverlay = new ActionCommand(() =>
                    {
                        this.WindowState = WindowState.Minimized;
                    }));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));

        }

        public DispatcherTimer AnimateDonation = new DispatcherTimer();
        public DispatcherTimer UpdateDonation = new DispatcherTimer();
        public DispatcherTimer AnimateTopDonation = new DispatcherTimer();
        public DispatcherTimer UpdateTopDonation = new DispatcherTimer();
        public DispatcherTimer AnimateViewMatches = new DispatcherTimer();
        public DispatcherTimer AnimateMatch = new DispatcherTimer();

        public Donations RecentDonations;
        public List<string> TopDonations;

        public MainWindow()
        {


            InitializeComponent();
            var myCur = Application.GetResourceStream(new Uri("pack://application:,,,/Elements/Cursor.cur")).Stream;
            Cursor = new Cursor(myCur);
            _time = TimeSpan.FromSeconds(0);
            RecentDonations = new Donations();
            RecentDonations.data = new List<Donation>();
            TopDonations = new List<string>();
            _timer = new DispatcherTimer(new TimeSpan(0, 0, 1), DispatcherPriority.Normal, delegate
            {
                NotifyPropertyChanged("Time");
                // lTimer.Content = _time.ToString("c");
                if (_time != TimeSpan.Zero)
                    _time = _time.Add(TimeSpan.FromSeconds(-1));
            }, Application.Current.Dispatcher);


            _timer.Start();
        }


        public void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyboardDevice.Modifiers == ModifierKeys.Control)
                switch (e.Key)
                {
                    case Key.Down:
                        TimeDown.Execute(null);
                        e.Handled = true;
                        return;

                    case Key.Up:
                        TimeUp.Execute(null);
                        e.Handled = true;
                        return;
                    case Key.Left:
                        Score1Up.Execute(null);
                        e.Handled = true;
                        return;

                    case Key.Right:
                        Score2Up.Execute(null);
                        e.Handled = true;
                        return;
                }
            if (Keyboard.Modifiers == (ModifierKeys.Control | ModifierKeys.Shift))
                switch (e.Key)
                {
                    case Key.Left:
                        Score1Down.Execute(null);
                        e.Handled = true;
                        return;

                    case Key.Right:
                        Score2Down.Execute(null);
                        e.Handled = true;
                        return;

                }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            if (needClose)
                Environment.Exit(0);
        }
    }
}
