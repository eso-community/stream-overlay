﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Groups
{
    public class MapPool
    {
        public int order { get; set; }
        public int map_id { get; set; }
    }

    public class Round
    {
        public string name { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public int best_of { get; set; }
        public List<MapPool> map_pool { get; set; }
    }

    public class Map
    {
        public string name { get; set; }
        public string minimap_image { get; set; }
    }

    public class Player
    {
        public int player_id { get; set; }
        public int position { get; set; }
        public int wins { get; set; }
        public int losses { get; set; }
    }

    public class Player2
    {
        public int player_id { get; set; }
        public int? score { get; set; }
        public bool? won { get; set; }
    }

    public class Match
    {
        public int id { get; set; }
        public List<Player2> players { get; set; }
        public List<Schedule> schedule { get; set; }
    }

    public class Schedule
    {
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string channel_name { get; set; }
        public List<string> casters { get; set; }
        public string type { get; set; }
        public int? player_id { get; set; }
    }

    public class Group
    {
        public int id { get; set; }
        public List<Player> players { get; set; }
        public List<Match> matches { get; set; }
    }

    public class Player3
    {
        public int id { get; set; }
        public string name { get; set; }
        public string flag_name { get; set; }
        public string flag_image { get; set; }
        public string avatar_image { get; set; }
        public double power_rating { get; set; }
        public double? elo { get; set; }
    }

    public class CurrentGroups
    {
        public string id { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public Round round { get; set; }
        public List<Map> maps { get; set; }
        public List<Group> groups { get; set; }
        public List<Player3> players { get; set; }
    }
}
